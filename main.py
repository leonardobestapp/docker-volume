#!/usr/bin/env python3

from flask import Flask
import sqlite3

app = Flask(__name__)

conn = sqlite3.connect("db-data/db.sqlite")
conn.close()

@app.route("/", methods=['GET', 'POST'])
def index():
    return "hello wei"


if __name__ == "__main__":
    app.run(host="0.0.0.0", port="5000", debug=True)