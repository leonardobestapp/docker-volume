FROM ubuntu:18.04

RUN apt-get update && \
    apt-get -y install python3 python3-pip

WORKDIR /my_first_app

COPY db-data /my_first_app
COPY main.py /my_first_app
COPY requirements.txt /my_first_app

RUN pip3 install -r requirements.txt

EXPOSE 5000

ENTRYPOINT ["python3", "main.py"]